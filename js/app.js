const images = [...document.querySelectorAll(".image-to-show")];
const pageBody = document.querySelector("body");
let previousActiveImage = document.querySelector(".active");
const imageWrapper = document.querySelector(".images-wrapper");

pageBody.addEventListener("click", onBtnClick);

let timer;
let offSet = 400;
makeVisibleImage();

function makeVisibleImage() {
  let i = images.indexOf(previousActiveImage) + 1;
  if (i > images.length-1) { i = 0; };

  previousActiveImage?.classList.remove("active");
  
  previousActiveImage = images[i];
 
  previousActiveImage? images[i].classList.add("active") : images[0].classList.add("active");
  timer = setTimeout(makeVisibleImage, 3000);

  offSet -= 400;
  if (offSet < -1200) {
    offSet = 0;
  }
  imageWrapper.style.left = offSet + "px";
}

function onBtnClick(e) {
  switch (e.target.dataset.action) {
    case "start":
      timer = setTimeout(makeVisibleImage, 3000);
      break;
    case "stop":
      clearTimeout(timer);
      break;
  
    default:
      break;
  }
}




